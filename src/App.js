import "./App.css";
// import EnrollmentForm from "./components/EnrollmentForm";
// import FormikContainer from "./components/FormikContainer";
import LoginForm from "./components/LoginForm";
// import RegistrationForm from "./components/RegistrationForm";
// import YoutubeForm from "./components/YoutubeForm";
import { Theme, ThemeProvider } from "@chakra-ui/core";

function App() {
  return (
    <ThemeProvider theme={Theme}>
      <div className="App">
        {/* <YoutubeForm /> */}
        {/* <FormikContainer /> */}
        <LoginForm />
        {/* <RegistrationForm /> */}
        {/* <EnrollmentForm /> */}
      </div>
    </ThemeProvider>
  );
}

export default App;
